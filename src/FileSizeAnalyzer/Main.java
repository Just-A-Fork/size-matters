package FileSizeAnalyzer;

import FileSizeAnalyzer.Classes.ErrorWin;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;

import java.io.File;
import java.net.URI;
import java.net.URL;

public class Main extends Application {

	@Override
	public void start(Stage stage) throws Exception {

		//load main scene
		try {
			File fxmlFile = new File(getClass().getClassLoader().getResource("Man.fxml").getFile());
			FXMLLoader fxml = new FXMLLoader();
		}catch(NullPointerException e){
			new ErrorWin("Missing File", "The file describing how to construct " +
					"the window layout is missing", true).Display();
		}
	}

	public static void main(String args[]){
		launch(args);
	}
}
